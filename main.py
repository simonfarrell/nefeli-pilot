#!/usr/bin/env python
# CLI wrapper to test the Nefeli API 
import argparse
import time
import os
# import urllib3
import json

# Import Utilities functions
from colt_nefeli_utils import ColtNefeliUtils

from model.colt_topology import ColtTopology
from model.tor_inventory import TORInnerVLANInventory

# Disable self-signed certificate Warnings
# urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def main():
    parser = argparse.ArgumentParser('Manage Nefeli Service Chains')
    args = parse_cli(parser)
    cu = ColtNefeliUtils()

    if args.file:
        colt_topology = ColtTopology.from_file(args.file)
        nefeli_components = translate_colt_to_nefeli(args.name, colt_topology)
    # If we get here then a file has been successfully loaded or we don't need one
    cu.login()
    
    if args.action == 'create':
        create_svc_chain(cu, nefeli_components)
    elif args.action == 'update':
        update_svc_chain(cu, nefeli_components)
    elif args.action == 'delete':
        delete_chain(cu, args.name)
    elif args.action == 'read':
        print_chain(cu, args.name)
    else:
        parser.print_usage()

def translate_colt_to_nefeli(name, colt_svc_def):
    # TODO: write this translation. For now, assume we are just using a dictionary like this,
    # which we have already read from disk as colt_svc_def. We just enrich it with a "name" entry
    # for the moment.
    """
    {
        "tunnels": [
            {
                "name": "OCN123(vlan_3006)"
                "vlan": 3000
            }
        ],
        "nf_data": {
            "JUNOS2": {
                "config": {
                    "interfaces": [
                        { 
                            "arp_entries": [
                                { 
                                    "ip": "10.0.0.2",
                                    "mac": "39:43:A7:6A:FB:96"
                                }
                            ], 
                            "name": "swan1",
                            "ip": "10.0.0.1"
                        },
                        { 
                            "name": "slan1",
                            "ip": "192.168.0.1"
                        }
                    ],
                    "routes": [
                        {
                            "prefix": "10.0.0.0/8",
                            "next_hop": "10.0.0.2"
                        }
                    ]
                },
                "credentials": {
                    "NFConfig": {
                            "Username": "nefeli",
                            "Password": "Nefeli",
                            "Port": "22",
                            "_comment": "BootTime: Maximum time in seconds for NF to boot",
                            "BootTime": "600",
                            "Plugin": "file:///opt/nefeli/plugins/juniper-vsrx/vsrx-config.py",
                            "PluginArgs": ["--source", "candidate", "--replace"]
                    },
                    "NFProbe": {
                            "Username": "nefeli",
                            "Password": "Nefeli",
                            "Port": "22",
                            "Plugin": "file:///opt/nefeli/plugins/juniper-vsrx/vsrx-config.py"
                    },
                    "NFLicense": {
                            "Username": "nefeli",
                            "Password": "Nefeli",
                            "Port": "22",
                            "Plugin": "file:///opt/nefeli/plugins/juniper-vsrx/vsrx-license.py"
                    }
                }
            }
        },
        "pipeline": {
            "nodes": {
                "CUST_LAN": {
                    "port": {
                        "tunnel": "OCN123(vlan_3006)"
                    },
                    "site": "flock"
                },
                "JUNOS2": {
                    "nf": {
                        "catalogId": "JUNOS2_NF",
                        "disableScaling": False,
                        "initialInstances": 0,
                        "minInstances": 0,
                        "maxInstances": 0,
                        "overprovisionInstances": 0,
                        "disableMetadata": False
                    },
                    "site": "flock"
                },
                "IPA_WAN": {
                    "port": {
                        "tunnel": "OCN123(vlan_3106)"
                    },
                    "site": "flock"
                }
            },
            "edges": [
                {
                    "a": {
                        "node": "CUST_LAN",
                        "interface": ""
                    },
                    "b": {
                        "node": "JUNOS2",
                        "interface": "lan"
                    },
                    "filterAb": {
                        "bpf": ""
                    },
                    "filterBa": {
                        "bpf": ""
                    }
                },
                {
                    "a": {
                        "node": "JUNOS2",
                        "interface": "wan"
                    },
                    "b": {
                        "node": "IPA_WAN",
                        "interface": ""
                    },
                    "filterAb": {
                        "bpf": ""
                    },
                    "filterBa": {
                        "bpf": ""
                    }
                }
            ],
        "maxRateKpps": "100000"
        }
    }
    """
    colt_svc_def['name'] = name
    return colt_svc_def


def create_svc_chain(cu, nefeli_components):
    # First create tunnels, then the pipeline, then configure it
    for tunnel in nefeli_components['tunnels']: 
        cu.create_tunnel(tunnel['connection_id'], tunnel['site'], tunnel['vlan_id'])

    sc = nefeli_components['pipeline']
    cu.create_unconfigured_service_chain(sc, nefeli_components['name'], nefeli_components['pool'])

    for nf_name, nf_def in sc['nodes']:
        if 'nf' in node_def:
            nf_data = sc['nf_data'][node_name]
            cu.configure_vnf_in_nefeli(sc['name'], nf_name, nf_def, nf_data)

def update_svc_chain(cu, nefeli_components):
    # We never update tunnels, and we rewrite service chains completely
    sc = nefeli_components['pipeline']
    cu.create_unconfigured_service_chain(sc, nefeli_components['name'], nefeli_components['pool'])
    for node_name, node in sc['nodes']:
        if 'nf' in node:
            cu.configure_vnf_in_nefeli(sc, sc['name'], node_name)

def delete_chain(cu, svc_name):
    components = cu.get_nefeli_svc(svc_name)
    cu.delete_service_chain(components['pipeline'])
    for tunnel in components['tunnels']:
        cu.delete_tunnel(tunnel['site'], tunnel['name'])

def print_chain(cu, svc_name):
    components = cu.get_nefeli_svc(svc_name)
    print('Service: ' + svc_name)
    print('Tunnels:')
    for tunnel in components.get_tunnels():
        tunnel.print()
    print('Service chain:')
    components.get_service_chain().print()

def delete_tunnel(cu, context, tunnel_name):
    cu.apis['tunnels'].delete_tunnel(context['tenant'], context['site'], tunnel_name)

def tor_inventory(cu, site):
    # TODO: working here
    colt_topology = ColtTopology.from_file('test/sample_ia_sc.json', cu.manifest_path)
    tor_inventory = load_inventory([(3000, 3050), (3100, 3150)], ['CUST', 'INTERNET'], 'flock')
    colt_topology.reserveTORVLANs(tor_inventory)

def load_inventory(vlan_ranges, vlan_prefixes, site_name):
    return TORInnerVLANInventory(site_name, vlan_ranges, vlan_prefixes)

def parse_cli(parser):
    parser.add_argument('-c', '--create', dest='action', action='store_const', const='create')
    parser.add_argument('-d', '--delete', dest='action', action='store_const', const='delete')
    parser.add_argument('-u', '--update', dest='action', action='store_const', const='update')
    parser.add_argument('-r', '--read', dest='action', action='store_const', const='read')
    parser.add_argument('-n', '--name', required=True)
    parser.add_argument('-f', '--file')
    args = parser.parse_args()
    return args

if __name__== "__main__":
  main()