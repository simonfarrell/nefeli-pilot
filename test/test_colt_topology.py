from __future__ import absolute_import

import unittest
import model
from model.colt_topology import ColtTopology

class TestColtTopology(unittest.TestCase):
    def setUp(self):
        file = open('test/sample_ia_sc.json', 'r')
        s = file.read()
        file.close()
        self.sample = ColtTopology.from_json(s, './manifests/')

    def tearDown(self):
        pass

    def testLoadJSON(self):
        self.assertTrue(self.sample.service_type != None)
        for vnf in self.sample.getVNFsWithType('VCPE'):
            print(vnf['interfaces'])

    def testGetVNFsWithType(self):
        vnf = self.sample.getVNFsWithType('VCPE')[0]
        self.assertTrue(vnf['id'] == 3)
        self.assertEqual(len(vnf['interfaces']), 2)

    def testGetConnectionById(self):
        self.assertTrue(self.sample.getConnectionById(1) != None)

    def testGetComponentById(self):
        self.assertTrue(self.sample.getComponentById(1) != None)

    def testGetConnsWithEndpointType(self):
        conns = self.sample.getConnsWithEndpointType('VCPE')
        self.assertEqual(len(conns), 2)
        self.assertTrue(conns[0]['attributes']['a-end-component'] == 3 or conns[0]['attributes']['b-end-component'] == 3)

    def testSetConnectionAttribute(self):
        self.sample.setAttributeForConnection(1, 'a-end-vlan', 99)
        self.assertEqual(self.sample.getAttributeForConnection(1, 'a-end-vlan'), 99)


if __name__ == '__main__':
    unittest.main()



