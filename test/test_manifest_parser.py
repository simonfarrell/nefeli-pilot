from __future__ import absolute_import

import unittest
import model
from model.manifest_parser import ManifestParser

class TestManifestParser(unittest.TestCase):

    def setUp(self):
        file = open('../nefeli-api-colt-examples/IPACCESS_VCPE_VNF/manifest.json', 'r')
        s = file.read()
        file.close()
        self.sample = ManifestParser.from_json(s)

    def tearDown(self):
        pass

    def testLoadJSON(self):
        self.assertTrue(self.sample.name != None)

    def testGetInterfacesByType(self):
        il = self.sample.getInterfaceIdsByType('DATA')
        print(il)
        self.assertTrue(len(il) == 2)
        il = self.sample.getInterfaceIdsByType('MGMT')
        print(il)
        self.assertTrue(len(il) == 1)



if __name__ == '__main__':
    unittest.main()



