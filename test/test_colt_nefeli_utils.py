# from __future__ import absolute_import
import os
import unittest
from unittest.mock import MagicMock
import sys
sys.modules['pynefeli'] = MagicMock()
sys.modules['pynefeli.rest'] = MagicMock()

from colt_nefeli_utils import ColtNefeliUtils

class TestColtNefeliUtils(unittest.TestCase):
    def setUp(self):
        os.environ['NEFELI_TENANT_ID'] = '1234'
        self.cu = ColtNefeliUtils()

    def tearDown(self):
        pass

    def test_init(self):
        self.assertEquals(self.cu.tenant['id'], '1234')

    def test_login(self):
        self.cu.login()
        self.assertIsNotNone(self.cu.apis['tunnels'])

    def test_raise_warning(self):
        msg = 'Test'
        try:
            self.cu.raiseRuntimeWarning(msg)
            self.fail('Exception not raised')
        except RuntimeWarning as rw:
            self.assertEqual(str(rw), msg)

    def test_load_vnf_configuration(self):
        try:
            os.mkdir('TEST_NF')
        except FileExistsError:
            pass

        files = ['config', 'credentials', 'filter', 'rollback', 'mapping']
        for f in files:
            self.writeFile('TEST_NF/' + f, 'txt')
        cfg = self.cu.load_vnf_configuration('TEST_NF')
        self.assertIsNotNone(cfg['configTemplate'])
        os.remove('TEST_NF/config')
        for f in [ff for ff in files if ff != 'config']:
            self.assertIsNotNone(cfg[f])
            os.remove('TEST_NF/' + f)
        os.removedirs('TEST_NF')

    def writeFile(self, path, content):
        out = open(path,"w")
        out.write(content)
        out.write("\n")
        out.close()

    def test_create_tunnel(self):
        self.assertTrue(True)
    

    def test_add_container_connection_point(self):
        svc_chain = {}
        sc = self.cu.add_container_connection_point(svc_chain, 'WAN', 'TESTSITE', 'TUNNEL_X')
        self.assertEquals(sc['nodes']['TUNNEL_X']['site'], 'TESTSITE')
        self.assertEquals(sc['nodes']['TUNNEL_X']['port']['tunnel'], 'WAN')

    def test_add_container_connection(self):
        svc_chain = {}
        sc = self.cu.add_container_connection(svc_chain, 'fromcmp', 'fromcon', 'tocmp', 'tocon')
        self.assertEquals(sc['edges'][0]['a']['node'], 'fromcmp')
        self.assertEquals(sc['edges'][0]['a']['interface'], 'fromcon')
        self.assertEquals(sc['edges'][0]['b']['node'], 'tocmp')
        self.assertEquals(sc['edges'][0]['b']['interface'], 'tocon')

    def test_add_vnf_to_service_chain(self):
        svc_chain = {}
        sc = self.cu.add_vnf_to_service_chain(svc_chain, 'MYVNF', 'TESTSITE', 'MY_CATALOG_NF')
        self.assertEquals(sc['nodes']['MYVNF']['site'], 'TESTSITE')
        self.assertEquals(sc['nodes']['MYVNF']['node']['nf']['nfCatalogId'], 'MY_CATALOG_NF')
    
