from enum import IntEnum
import logging
import logging.handlers as handlers
import os
# Import Nefeli Python SDK
import pynefeli as nefeli
from pynefeli.rest import ApiException

import jinja2
import urllib3
from pprint import pprint

class ColtNefeliUtils():

    def __init__(self, manifest_directory = None):
        self.password_file_default = 'config.txt'
        self.apis = {}
        self.tenant = {}
        self.tenant['id'] = os.environ.get('NEFELI_TENANT_ID') or '148471601712245161278117758220014262984'
        self.tenant['name'] = os.environ.get('NEFELI_TENANT_NAME') or 'nefeli'
        self.manifest_path = manifest_directory or './manifests'
        self.templates_dir = './templates'
        self.init_logger()

    def login(self):
        host = os.environ.get('NEFELI_API_HOST') or 'https://colt-weaver.staging.services.nefeli.io'
        un = os.environ.get('NEFELI_USERNAME') or 'colt'
        pw = os.environ.get('NEFELI_PASSWORD') or self.read_config(self.password_file_default)
        self.initialize_apis(host, un, pw)
        # Disable self-signed certificate Warnings
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    def read_config(self, fname):
        file = open(fname, 'r')
        cfg = file.read()
        file.close()
        return cfg

    def initialize_apis(self, host, username, password):
        ## SDK Configuration
        configuration = nefeli.Configuration()
        configuration.host = host
        configuration.username = username
        configuration.password = password
        configuration.verify_ssl = False
  
        self.apis['users'] = nefeli.UsersApi(nefeli.ApiClient(configuration))
        try:
            # Login to Nefeli Weaver using credentials passed in configuration
            login_data = self.apis['users'].login()
        except ApiException as e:
            print("Exception when logging in to Nefeli Weaver: %s\n" % e)

        # Update configuration with Bearer Token received during login phase
        configuration.api_key['Authorization'] = login_data.token
        configuration.api_key_prefix['Authorization'] = 'Bearer'

        self.apis['tenants'] = nefeli.TenantsApi(nefeli.ApiClient(configuration))
        self.apis['tunnels'] = nefeli.TunnelsApi(nefeli.ApiClient(configuration))
        self.apis['services'] = nefeli.ServicesApi(nefeli.ApiClient(configuration))
        self.apis['files'] = nefeli.MnemeApi(nefeli.ApiClient(configuration))
        self.apis['nfs'] = nefeli.NetworkFunctionsApi(nefeli.ApiClient(configuration))

    def raiseRuntimeWarning(self, msg):
        self.logger.error(msg)
        raise RuntimeWarning(msg)

    def init_logger(self):
        self.logger = logging.getLogger('colt-nefeli')
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        logHandler = handlers.TimedRotatingFileHandler('timed_app.log', when='M', interval=1, backupCount=2)
        logHandler.setLevel(logging.INFO)
        logHandler.setFormatter(formatter)
        self.logger.addHandler(logHandler)

    def create_tunnel(self, connection_id, site, vlan_id):
        tunnel = {
            'tunnel': {
                'encap': {
                    'ivid': vlan_id,
                    'ovid': 0
                }
            },
            'name': connection_id,
            'site': site,
            'tenant': self.tenant['name']
        }
        self.apis['tunnels'].post_tunnel(tunnel)

    def load_vnf_configuration(self, config_data, credentials, nf_catalog_id):
        nf_configuration = {}
        tm = self.loadJinjaTemplate(nf_catalog_id)
        configText = tm.render(cfg=config_data)
        nf_configuration['configTemplate'] = configText
        nf_configuration['credentials'] = credentials
        #TODO: test if Nefeli works with a blank filter
        # with open('%s/filter' %nf_catalog_id, 'r') as flt:
        #     nf_configuration['filter'] = flt.read()
        nf_configuration['filter'] = ''
        nf_configuration['rollback'] =  '{"Rollback": {"_comment": "On config failure, reboot the instance and try again.", "Policy": "reboot" } }'
        nf_configuration['mapping'] = ''       
        return nf_configuration

    
    def loadJinjaTemplate(self, fname):
        templateLoader = jinja2.FileSystemLoader(searchpath=self.templates_dir)
        templateEnv = jinja2.Environment(loader=templateLoader)
        return templateEnv.get_template(fname)

    def add_container_connection_point(self, service_chain, connection_id, site_name, container_connection_id):
        if not 'nodes' in service_chain.keys():
            service_chain['nodes'] = {}
        service_chain['nodes'][container_connection_id] = {
            'site': site_name,
            'port': {
                'tunnel': connection_id
            }
        }
        return service_chain

    def add_container_connection(self, service_chain, source_component, source_connection, destination_component, destination_connection):
        if not 'edges' in service_chain:
            service_chain['edges'] = []
        service_chain['edges'].append({
            'a': {
                'node': source_component,
                'interface': source_connection
            },
            'b': {
                'node': destination_component,
                'interface': destination_connection
            },
            'filterAb': {
                'bpf': ''
            },
            'filterBa': {
                'bpf': ''
            }
        })
        return service_chain

    def add_vnf_to_service_chain(self, service_chain, vnf_id, site_id, nf_catalog_id):
        if not 'nodes' in service_chain.keys():
            service_chain['nodes'] = {}
        service_chain['nodes'][vnf_id] = {
            'site': site_id,
            'node': {
                'nf': {
                    'nfCatalogId': nf_catalog_id
                }
            }
        }
        return service_chain

    # This method relies on a service chain having only one license pool. We have asked Nefeli to change this.
    def create_unconfigured_service_chain(self, service_chain, service_chain_id, license_pool):
        # Create Service Chain on Nefeli Weaver
        self.apis['services'].post_pipeline(self.tenant['id'], service_chain_id, service_chain, pool_id=license_pool, force=True)

    # This method will (re)start the VNF immediately and is also not ideal because configuration and start should be independent. 
    # We have asked Nefeli to change it. It also relies on loading a config from a directory instead of using
    # data supplied in a ColtTopology. TODO: integrate parsing of the NF Manifest and the ColtTopology to create an NF config
    def configure_vnf_in_nefeli(self, service_chain_id, vnf_id, vnf_def, vnf_data):              
        config_data = vnf_data['config']
        credentials = vnf_data['credentials']
        nf_catalog_id = vnf_def['nf']['catalogId']
        cfg = self.load_vnf_configuration(config_data, credentials, nf_catalog_id)
        # Update the service chain on Nefeli
        self.apis['services'].post_pipeline_nf_config(self.tenant['id'], service_chain_id, vnf_id, cfg)
 
    # Assemble a {pipeline:, tunnels:, name: } dictionary. TODO: make this a class
    def get_nefeli_svc(self, service_chain_id):
        pipeline = self.apis['services'].get_pipeline_id(self.tenant['id'], service_chain_id)
        sc = { 'pipeline': pipeline, 'tunnels': [], 'name': service_chain_id}
        nodes = pipeline['nodes']
        for tunnel in [t for t in nodes if 'nf' not in t]:
            site = nodes[tunnel]['site']
            tname = nodes[tunnel]['port']['tunnel']
            td = self.get_tunnel_definition(site, tname)
            sc['tunnels'].append({'name': tunnel, 'site': site, 'vlan_id': td['tunnel']['encap']['ivid']})
        return sc

    def get_tunnel_definition(self, site, tunnel_name):
        ts = self.apis['tunnels'].get_tunnels(tenant=self.tenant['id'], site=site, tunnel=tunnel_name)
        return ts['tenantTunnels'][0]

    def delete_service_chain(self, svc_id):
        self.apis['services'].delete_pipeline(self.tenant['id'], svc_id)
    
    def delete_tunnel(self, site, tunnel_name):
        self.apis['tunnels'].delete_tunnel(site, tunnel_name)