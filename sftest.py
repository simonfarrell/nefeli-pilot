#!/usr/bin/env python

from __future__ import print_function
import time
import os
import urllib3
import json

# Import Utilities functions
import colt_nefeli_utils as cu
import nefeli_utils as nu

# Import Nefeli Python SDK
import pynefeli as nefeli

from model.colt_topology import ColtTopology
from model.tor_inventory import TORInnerVLANInventory

# Disable self-signed certificate Warnings

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def delete_tunnel(apis, context, tunnel_name):
    apis['tunnels'].delete_tunnel(context['tenant'], context['site'], tunnel_name)

def create_tunnel(apis, tenant, site, customer, vlan_id):
    # TODO
    # Add existence check
    #
    vlan_name = f'{customer}_{vlan_id}' 
    nu.create_connection(apis['tunnels'], tenant, vlan_name, site, vlan_id)
    return vlan_name

    
# def getNefeliSite(vnf):
#     return vnf['attributes']['nefeli_site']

# def createServiceChain(colt_topology):
#     ct = colt_topology.allocateTORPorts()
#     print(ct)


    # L2 to Customer LAN: ETH_1
    # L2 to Internet Router ID: IPA_1

    # service_chain = {
    #     'edges': [
    #         {
    #             'edge': {
    #                 'aGate': '', 
    #                 'aNode': 'SC_PORT_1',
    #                 'bGate': 'VCPE_CUST_PORT',
    #                 'bNode': 'VNF_1',
    #                 'dirAb': {},
    #                 'dirBa': {}
    #             }
    #         },
    #         {
    #             'edge': {
    #                 'aGate': '',
    #                 'aNode': 'SC_PORT_2',
    #                 'bGate': 'VCPE_INTERNET_PORT',
    #                 'bNode': 'VNF_1',
    #                 'dirAb': {},
    #                 'dirBa': {}
    #             }
    #         }
    #     ],
    #     'nodes': {
    #         'SC_PORT_1': {
    #             'site': 'flock',
    #             'node': {
    #                 'port': {
    #                     'port': {},
    #                     'tunnel': 'ETH_1'
    #                 }
    #             }
    #         },
    #         'SC_PORT_2': {
    #             'site': 'flock',
    #             'node': {
    #                 'port': {
    #                     'port': {},
    #                     'tunnel': 'IPA_1'
    #                 }
    #             }
    #         },
    #         'VNF_1': {
    #             'site': 'flock',
    #             'node': {
    #                 'nf': {
    #                     'nfCatalogId': 'IPACCESS_VCPE_VNF'
    #                 }
    #             }
    #         }
    #     }
    # }

def translateColtTopologyToNefeli(colt_topology):
    vnfs = [{name: v.name, typ: 'IPACCESS_VCPE_VNF'} for v in colt_topology.getVNFsWithType('VCPE')]
    tunnels = 


def create_sc(apis, tenant, site, ocn, colt_topology, inventory):
    service_chain = {
        'nodes': {},
        'edges': []
    }

    svc_name = colt_topology.service_name
    vnf_pool = 'vcpe_pool'

    for vnf in colt_topology.getVNFsWithType('VCPE'):
        vnf_name = colt_topology.getAttributeForComponent(vnf['id'], 'name')
        vnf_type = 'IPACCESS_VCPE_VNF'
        service_chain = nu.add_vnf_to_service_chain(apis, service_chain, tenant, svc_name, vnf_name, site, vnf_type, vnf_pool) 

    for tunnel in colt_topology.getVNFEdgePorts():
        vlan_name = create_tunnel(apis, tenant, site, ocn, tunnel.vlan_id)
        service_chain = nu.add_container_connection_point(apis, service_chain, tenant, svc_name, tunnel.name, site, vlan_name) 
        vnf = colt_topology.findVNFForTORVLAN(tunnel.id)
        vnf_name = colt_topology.getAttributeForComponent(vnf['id'], 'name')
        vnf_port_name = getVNFPortName(vnf, tunnel)
        service_chain = nu.add_container_connection(apis, service_chain, tenant, svc_name, vnf_name, vnf_port_name, vlan_name,'')

    for conn in colt_topology.getInterVNFConnections():
        service_chain = nu.add_container_connection(apis, service_chain, tenant, svc_name, 
                        conn['vnfs'][0]['name'], conn['vnfs'][0]['port_name'], conn['vnfs'][1]['name'], conn['vnfs'][1]['port_name'])


    # # Create Service Chain on Nefeli Weaver
    apis['services'].post_pipeline(tenant, svc_name, service_chain, pool_id=vnf_pool)
    
    # # Load configuration form VNF_1 directory
    # apis['services'].post_pipeline_nf_config(tenant.id, 'SC_1', 'VNF_1', utils.load_configuration('IPACCESS_VCPE_VNF'), gexp=True)



def load_inventory(apis, vlan_ranges, vlan_prefixes, site_name):
    return TORInnerVLANInventory(site_name, vlan_ranges, vlan_prefixes)

def main():
    (apis, context) = cu.login()
    colt_topology = ColtTopology.from_file('test/sample_ia_sc.json', context['manifest_path'])
    tor_inventory = load_inventory(apis, [(3000, 3050), (3100, 3150)], ['CUST', 'INTERNET'], 'flock')
    colt_topology.reserveTORVLANs(tor_inventory)
    # nefeli_sc = translateColtTopologyToNefeli(colt_topology)
    # create_sc(apis, context['tenant'], nefeli_sc, 'flock', colt_topology.ocn, tor_inventory)

    # print('Sleeping')
    # time.sleep(10)

    # delete_tunnel(apis, context, vlan_cust)
    # delete_tunnel(apis, context, vlan_internet)



if __name__== "__main__":
  main()