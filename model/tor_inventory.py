from __future__ import absolute_import


class TORInnerVLANInventory(object):
    def __init__(self, site, vlan_ranges, vlan_prefixes):
        self.site = site
        self.vlan_ranges = vlan_ranges
        self.used = [n for n in range(0,4096)]
        self.reserved = set()

    def syncInventory(self, apis):
        tl = apis['tunnels'].get_tunnels(self.site)
        tunnels = tl.to_dict()['tenant_tunnels']
        self.used = [t['tunnel']['encap']['ivid'] for t in tunnels]

    def reserveVLAN(self, vlan_id):
        self.reserved.add(vlan_id)

    def unreserveVLAN(self, vlan_id):
        self.reserved.remove(vlan_id)
    
    # Look for first free value based on pre-defined ranges
    def nextFreeVLAN(self, tunnel_type):
        tup = [self.vlan_ranges][int(tunnel_type)]
        for n in range(tup[0], tup[1]):
            if n not in self.used and n not in self.reserved:
                return n
        return None
    
    def translatePortTypeToTunnelType(self, farend_type):
        if farend_type == 'Internet Port':
            return 'INTERNET'
        elif farend_type == 'Ethernet Port':
            return 'CUST'
        else:
            raise Exception(f'No TOR VLAN group translation for connection to {farend_type}')
    

