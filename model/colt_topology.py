import json

from model.manifest_parser import ManifestParser

class ColtTopology(object):
    def __init__(self, adict, manifest_dir):
        self.manifest_dir = manifest_dir
        self.components = adict['components']
        self.connections = adict['connections']
        self.ocn = adict['ocn']
        self.action = adict['action']
        self.service_type = adict['service_type']
        self.service_name = adict['service_name']
        self.enrich_vnfs_from_manifest()

    @staticmethod
    def from_json(s, config):
        return ColtTopology(json.loads(s), config)
    
    @staticmethod
    def from_file(fname, config):
        file = open(fname, 'r')
        s = file.read()
        file.close()
        return ColtTopology.from_json(s, config)

    def getVNFsWithType(self, service_type):
        return [c for c in self.components if c['service_type'] == service_type]

    def getConnsWithEndpointType(self, service_type):
        components = self.getVNFsWithType(service_type)
        endpoints = []
        for c in components:
            endpoints = endpoints + self.getConnectionsByComponentId(c['id'])
        return endpoints

    def getConnectionsByComponentId(self, id):
        return [c for c in self.connections if c['attributes']['a-end-component'] == id or c['attributes']['b-end-component'] == id]

    def getConnectionById(self, id):
        for c in self.connections:
            if c['id'] == id:
                return c
        return None

    def getComponentById(self, id):
        for c in self.components:
            if c['id'] == id:
                return c
        return None

    def getOtherComponentForConnection(self, conn_id, this_end):
        if self.getAttributeForConnection(conn_id, 'a-end-component') == this_end:
            return self.getAttributeForConnection(conn_id, 'b-end-component')
        elif self.getAttributeForConnection(conn_id, 'b-end-component') == this_end:
            return self.getAttributeForConnection(conn_id, 'a-end-component')
        else:
            raise Exception(f'Cannot find other component for connection {conn_id} and component {this_end}')         

    def getAttributeForComponent(self, id, att):
        c = self.getComponentById(id)
        if att == 'action':
            return c['action'] 
        elif att == 'service_type':
            return c['service_type']
        else:
            return c['attributes'][att]

    def getAttributeForConnection(self, conn_id, att):
        c = self.getConnectionById(conn_id)
        if att == 'action':
            return c['action'] 
        elif att == 'service_type':
            return c['service_type']
        else:
            return c['attributes'][att]

    def setAttributeForConnection(self, conn_id, att, val):
         c = self.getConnectionById(conn_id)
         c['attributes'][att] = val

    # Find Ethernet connections in the Colt Topology that reference a VNF
    # Also extract the site
    # If the action is CREATE, then assign VLANs
    def allocateTORPorts(self, tor_inventory):
        vnfs = self.getVNFsWithType('VCPE')
        for vnf in vnfs:
            vnf_id = vnf['id']
            for c in self.getConnectionsByComponentId(vnf_id):
                self.updateTopologyWithTORDetailsForConnection(vnf_id, c['id'], tor_inventory)              

    def updateTopologyWithTORDetailsForConnection(self, vnf_id, conn_id, inventory):
        if self.getAttributeForConnection(conn_id,'action') == 'CREATE':
            self.updateTopologyVLANForConnectionEnd(conn_id, vnf_id, 'a', inventory)
            self.updateTopologyVLANForConnectionEnd(conn_id, vnf_id, 'b', inventory)

    def updateTopologyVLANForConnectionEnd(self, conn_id, vnf_id, abend, inventory):
        conn_attr = abend+'-end-connection'
        vlan_attr = abend+'-end-vlan'
        # If the topology has our VNF as the "abend" of this connection, we need to reserve a TOR VLAN
        # Otherwise, skip it
        if self.getAttributeForConnection(conn_id,conn_attr) == vnf_id:
            # Only assign a VLAN and create a tunnel if the topology is missing one
            if self.getAttributeForConnection(conn_id,vlan_attr) == -1:
                port_id = self.getOtherComponentForConnection(conn_id, vnf_id)
                conn_family = self.getAttributeForComponent(port_id, 'service_type')
                (vlan_id, vlan_name, _) = inventory.reserveVLAN(self.ocn, conn_family)
                self.setAttributeForConnection(id, vlan_attr, vlan_id)
                self.setAttributeForConnection(id, 'vlan_name', vlan_name)

    def enrich_vnfs_from_manifest(self):
        vnfs = self.getVNFsWithType('VCPE')
        for vnf in vnfs:
            man = vnf['attributes']['vcpe_manifest']
            path = self.manifest_dir + man + '.json'
            mp = ManifestParser.from_file(path)
            if_names = mp.getInterfaceIdsByType('DATA')
            vnf['interfaces'] = if_names

