import json

class ManifestParser(object):
  def __init__(self, adict):
    self.name = adict['name']
    self.interfaces = adict['components']['datapath']['vmManifest']['interfaces']

  @staticmethod
  def from_json(s):
    return ManifestParser(json.loads(s))
    
  @staticmethod
  def from_file(fname):
    file = open(fname, 'r')
    s = file.read()
    file.close()
    return ManifestParser.from_json(s)
  
  def getInterfaceIdsByType(self, ty):
    return [i['id'] for i in self.interfaces if i['type'] == ty]
